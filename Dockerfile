FROM placeholder-will-be-replaced-with-CI-variable

# local and envs
ENV PIP_ROOT_USER_ACTION=ignore
ARG DEBIAN_FRONTEND=noninteractive
USER root

# setup
WORKDIR /DJ
COPY . /DJ/displaced-jets-analysis

# build the code
RUN mkdir /DJ/build && \
    cd /DJ/build && \
    source /release_setup.sh && \
    rm -vf ../displaced-jets-analysis/easyjet/CMakeLists.txt && \
    cmake ../displaced-jets-analysis && \
    make

CMD source /release_setup.sh && source  /DJ/build/x*/setup.sh && /bin/bash
