/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "VertexDecoratorAlg.h"
#include "AthContainers/AuxElement.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "Math/GenVector/VectorUtil.h"
#include "Math/Vector4D.h"
#include "StoreGate/ReadDecorHandle.h"
#include <AthContainers/ConstDataVector.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/VertexContainer.h>
#include "PathResolver/PathResolver.h"

#include "TH3S.h"
#include "TFile.h"

namespace DJ
{
  VertexDecoratorAlg ::VertexDecoratorAlg(const std::string &name,
                                          ISvcLocator *pSvcLocator)
      : AthHistogramAlgorithm(name, pSvcLocator)
  {
  }

  StatusCode VertexDecoratorAlg ::initialize()
  {
    // containers
    ATH_CHECK(m_vertexContainerKey.initialize());
    ATH_CHECK(m_eventInfoKey.initialize());

    // decorators
    for (std::string var : m_vars_float)
    {
      SG::AuxElement::Decorator<float> deco(var + m_deco_suffix);
      m_decos_float.emplace(var, deco);
    }
    for (std::string var : m_vars_int)
    {
      SG::AuxElement::Decorator<int> deco(var + m_deco_suffix);
      m_decos_int.emplace(var, deco);
    }

    
    if(m_add_materialVetoFlag){
      // Read the root file containing the material maps. They can be placed in the data/
      // or share/ directory of the package: PathResolver will find them.
      TString innerMapPath =  PathResolverFindCalibFile(m_matMapInnerFile);
      if( innerMapPath=="") return StatusCode::FAILURE;
      TString outerMapPath =  PathResolverFindCalibFile(m_matMapOuterFile);
      if( outerMapPath=="") return StatusCode::FAILURE;

      TFile* mapIn = new TFile(innerMapPath, "READ");
      TFile* mapOut = new TFile(outerMapPath, "READ");

      if( mapIn->IsOpen() && mapOut->IsOpen() ) {
	mapOut ->GetObject(m_matMapOuterName.value().c_str(), m_materialMapOuter);
	mapIn->GetObject(m_matMapInnerName.value().c_str(), m_materialMapInner);
	mapIn ->GetObject(m_matMapMatrixName.value().c_str(), m_materialMapMatrix);

	if(m_materialMapOuter) {
	  m_materialMapOuter->SetDirectory(0);
	}
	if(m_materialMapInner) {
	  m_materialMapInner->SetDirectory(0);
	}
	ATH_MSG_DEBUG( "Material Map Files Open." );
      }

      mapIn->Close();
      mapOut->Close();

      delete mapIn;
      delete mapOut;
    } // m_add_materialVetoFlag
    
    return StatusCode::SUCCESS;
  }

  StatusCode VertexDecoratorAlg ::execute()
  {
    // container we read in
    SG::ReadHandle<xAOD::EventInfo> eventInfo(m_eventInfoKey);
    ATH_CHECK(eventInfo.isValid());

    SG::ReadHandle<ConstDataVector<xAOD::VertexContainer>> vertexHandle(
        m_vertexContainerKey);
    ATH_CHECK(vertexHandle.isValid());

    ConstDataVector<xAOD::VertexContainer> vertices = *vertexHandle;

    xAOD::Vertex::ConstAccessor<float> chi2("chiSquared");
    xAOD::Vertex::ConstAccessor<float> nDoF("numberDoF");
    xAOD::Vertex::ConstAccessor<xAOD::Vertex::TrackParticleLinks_t> trkAcc(
        "trackParticleLinks");

    float minD0 = std::numeric_limits<float>::max();
    float maxD0 = 0;

    float HT = 0;

    float charge = 0;

    using FourVector_t =
        ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<Double_t>>;

    for (const xAOD::Vertex *vertex : vertices)
    {
      const xAOD::Vertex::TrackParticleLinks_t &trkParts = trkAcc(*vertex);
      size_t ntrk = trkParts.size();

      FourVector_t vectorialTrackSum;
      float maxDeltaR = 0;

      for (const auto &trklink : trkParts)
      {
        if (!trklink.isValid())
        {
          continue;
        }
        const xAOD::TrackParticle &trk = **trklink;

        double trk_d0 = std::abs(trk.definingParameters()[0]);
        if (trk_d0 < minD0)
        {
          minD0 = trk_d0;
        }
        if (trk_d0 > maxD0)
        {
          maxD0 = trk_d0;
        }

        xAOD::TrackParticle::ConstAccessor<float> ptAcc("pt_wrtSV");
        xAOD::TrackParticle::ConstAccessor<float> etaAcc("eta_wrtSV");
        xAOD::TrackParticle::ConstAccessor<float> phiAcc("phi_wrtSV");

        FourVector_t trk_P4(ptAcc(trk), etaAcc(trk), phiAcc(trk),
                            trk.p4().M());

        vectorialTrackSum += trk_P4;
        HT += ptAcc(trk);
        charge += trk.charge();

        FourVector_t vertexOneTrackRemoved;

        for (const auto &trklink_ref : trkParts)
        {
          if (trklink_ref == trklink)
            continue;

          if (!trklink_ref.isValid())
          {
            continue;
          }
          const xAOD::TrackParticle &trk_ref = **trklink_ref;
          FourVector_t trk_ref_P4(ptAcc(trk_ref), etaAcc(trk_ref),
                                  phiAcc(trk_ref), trk_ref.p4().M());
          vertexOneTrackRemoved += trk_ref_P4;
        }

        float deltaR =
            ROOT::Math::VectorUtil::DeltaR(vertexOneTrackRemoved, trk_P4);
        if (deltaR > maxDeltaR)
        {
          maxDeltaR = deltaR;
        }
      }

      
      // vertex covariance and Lxy_err
      const AmgSymMatrix(3) &covariance = vertex->covariancePosition();
      Amg::Vector3D position_xy = vertex->position();
      position_xy[2] = 0;
      float Lxy_err = std::sqrt(position_xy.dot(covariance * position_xy) /
                                position_xy.mag());

      SG::AuxElement::Decorator<FourVector_t> fourVectorDec("p4");

      fourVectorDec(*vertex) = vectorialTrackSum;

      m_decos_float.at("r")(*vertex) = vertex->position().mag();

      m_decos_float.at("Lxy")(*vertex) = vertex->position().perp();
      m_decos_float.at("Lxy_err")(*vertex) = Lxy_err;
      m_decos_float.at("Lxy_sig")(*vertex) =
	vertex->position().perp() / Lxy_err;

      m_decos_float.at("pt")(*vertex) = vectorialTrackSum.Pt();
      m_decos_float.at("eta")(*vertex) = vectorialTrackSum.Eta();
      m_decos_float.at("phi")(*vertex) = vectorialTrackSum.Phi();
      m_decos_float.at("m")(*vertex) = vectorialTrackSum.M();

      m_decos_float.at("HT")(*vertex) = HT;

      m_decos_float.at("maxDeltaR")(*vertex) = maxDeltaR;
      m_decos_float.at("mu")(*vertex) = vectorialTrackSum.M() / maxDeltaR;

      m_decos_float.at("mind0")(*vertex) = minD0;
      m_decos_float.at("maxd0")(*vertex) = maxD0;
      m_decos_float.at("charge")(*vertex) = charge;

      m_decos_int.at("ntrk")(*vertex) = ntrk;

      if(m_add_materialVetoFlag){
	    m_decos_int.at("passMatVeto")(*vertex) =  passMaterial( vertex);
      }

    }

    return StatusCode::SUCCESS;
  }


  int VertexDecoratorAlg::passMaterial(const xAOD::Vertex* vtx  ){
      
    bool passMaterialVeto_v3 = false;      
    const TVector3 dv_pos( vtx->x(), vtx->y(), vtx->z() );
      
    if (dv_pos.Perp() > 150){
      passMaterialVeto_v3 = ( m_materialMapOuter->GetBinContent(m_materialMapOuter->FindBin( dv_pos.Perp() ,
											     dv_pos.Phi() ,
											     dv_pos.z() ) ) == 0);
    }
    else {
      for (Int_t i=0;i<5;i++){
	if (dv_pos.Perp() < (*m_materialMapMatrix)[i][0]){
	  float test_x = dv_pos.x() + (*m_materialMapMatrix)[i][1];
	  float test_y = dv_pos.y() + (*m_materialMapMatrix)[i][2];
	  double calc_phi = fmod( TMath::ATan2(test_y,test_x),TMath::Pi()/(*m_materialMapMatrix)[i][3] );
	  if (calc_phi <0) calc_phi = calc_phi + TMath::Pi()/(*m_materialMapMatrix)[i][3];
	    
	  passMaterialVeto_v3 = ( m_materialMapInner->GetBinContent(m_materialMapInner->FindBin( sqrt(test_x*test_x + test_y*test_y ) ,  calc_phi ,  dv_pos.z() ) ) == 0);
	  break;
	}
      }
    } 

    return int(passMaterialVeto_v3);
    
  }
  




  int VertexDecoratorAlg::passMaterialStrict(const xAOD::Vertex* vtx  ){

      // Add 'stricter' material map veto
      // All bins in +/- Rxy, phi, and z directions are checked for presence of material
      // Granularity of material map was taken from DV+mu internal note (https://cds.cern.ch/record/2633973/files/ATL-COM-PHYS-2018-1138.pdf)
      // Unlike normal material map veto, stricter material map veto assumes DV passes flag (=1), unless material is found in bin

      bool passMaterialVeto_strict = true;

      const TVector3 dv_pos( vtx->x(), vtx->y(), vtx->z() );

      if (dv_pos.Perp() > 150){
	std::vector<double> dR_values = {-0.8, 0.0, 0.8};
	std::vector<double> dPhi_values = {-0.014477, 0.0, 0.014477};
	std::vector<double> dz_values = {-2.0, 0.0, 2.0};

	for (auto & dR : dR_values){
	  for (auto & dPhi : dPhi_values){
	    for (auto & dz : dz_values){

	      if (m_materialMapOuter->GetBinContent(m_materialMapOuter->FindBin( dv_pos.Perp()+dR , dv_pos.Phi()+dPhi , dv_pos.z()+dz ) ) == 1) passMaterialVeto_strict = 0;

	    }
	  }
	}

      }
      else {
	for (Int_t i=0;i<5;i++){
	  if (dv_pos.Perp() < (*m_materialMapMatrix)[i][0]){
	    float test_x = dv_pos.x() + (*m_materialMapMatrix)[i][1];
	    float test_y = dv_pos.y() + (*m_materialMapMatrix)[i][2];
	    float test_rxy = sqrt(test_x*test_x + test_y*test_y);
	    double calc_phi = fmod( TMath::ATan2(test_y,test_x),TMath::Pi()/(*m_materialMapMatrix)[i][3] );
	    if (calc_phi <0) calc_phi = calc_phi + TMath::Pi()/(*m_materialMapMatrix)[i][3];
	    
	    std::vector<double> dR_values = {-0.2, 0.0, 0.2};
	    std::vector<double> dPhi_values = {-0.007427, 0.0, 0.007427};
	    std::vector<double> dz_values = {-1.0, 0.0, 1.0};

	    for (auto & dR : dR_values){
	      for (auto & dPhi : dPhi_values){
		for (auto & dz : dz_values){

		  if (m_materialMapInner->GetBinContent(m_materialMapInner->FindBin( test_rxy+dR , calc_phi+dPhi , dv_pos.z()+dz ) ) == 1) passMaterialVeto_strict = 0;

		}
	      }
	    }
	    break;
	  }
	}
      } 
      
      return int(passMaterialVeto_strict);
    }

}
