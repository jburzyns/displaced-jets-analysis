/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// GNNDecoratorAlg.h
//
// This is an algorithm to decorate reco-level jets with
// their EJ GNN score
//
// Author: Jackson Burzynski <jackson.carl.burzynski@cern.ch>
///////////////////////////////////////////////////////////////////

// Always protect against multiple includes!
#ifndef EJSANALYSIS_GNNDECORATORALG
#define EJSANALYSIS_GNNDECORATORALG

#include <AthContainers/ConstDataVector.h>
#include <AthenaBaseComps/AthHistogramAlgorithm.h>
#include <FlavorTagDiscriminants/GNNTool.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/VertexContainer.h>

namespace DJ
{

  /// \brief An algorithm for decorating jets with custom observables
  class GNNDecoratorAlg final : public AthHistogramAlgorithm
  {
public:
    /// \brief The standard constructor
    GNNDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// @brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute() override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

    // configurables
private:
    /// \brief Setup syst-aware input container handles
    CP::SysListHandle m_systematicsList{this};

    /// @brief the name of the jet container to use
    CP::SysReadHandle<xAOD::JetContainer> m_jetContainerHandle{
        this, "jetContainerKey", "", "xAOD::JetContainer to read"};
    /// @brief the name of the EventInfo object
    SG::ReadHandleKey<xAOD::EventInfo> m_eventInfoKey{
        this, "eventInfoKey", "EventInfo", "EventInfo container to use"};

    /// @brief the suffix to add to the decorations
    Gaudi::Property<std::string> m_deco_suffix{
        this, "suffix", "", "Suffix to add after the decoration"};

    /// @brief the pre-configured GNNTool to use
    ToolHandle<FlavorTagDiscriminants::GNNTool> m_gnnTool{
        this, "gnnTool", "", "GNN Decorator tool"};
  };
}

#endif
