/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#include "GNNDecoratorAlg.h"
#include "AthContainers/AuxElement.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include "Math/GenVector/VectorUtil.h"
#include "StoreGate/ReadDecorHandle.h"
#include <AthContainers/ConstDataVector.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/TrackParticleContainer.h>
#include <xAODTracking/VertexContainer.h>

namespace DJ
{
  GNNDecoratorAlg ::GNNDecoratorAlg(const std::string &name,
                                    ISvcLocator *pSvcLocator)
      : AthHistogramAlgorithm(name, pSvcLocator)
  {
  }

  StatusCode GNNDecoratorAlg ::initialize()
  {
    // containers
    ATH_CHECK(m_jetContainerHandle.initialize(m_systematicsList));
    ATH_CHECK(m_eventInfoKey.initialize());

    // Intialise syst list (must come after all syst-aware inputs and outputs)
    ATH_CHECK(m_systematicsList.initialize());

    // tools
    ATH_CHECK(m_gnnTool.retrieve());

    return StatusCode::SUCCESS;
  }

  StatusCode GNNDecoratorAlg ::execute()
  {
    // container we read in
    SG::ReadHandle<xAOD::EventInfo> eventInfo(m_eventInfoKey);
    ATH_CHECK(eventInfo.isValid());

    // Loop over all systs
    for (const auto &sys : m_systematicsList.systematicsVector())
    {

      // jet container we read in
      const xAOD::JetContainer *jetContainer = nullptr;
      ATH_CHECK(m_jetContainerHandle.retrieve(jetContainer, sys));

      // apply the GNNTool to the jets
      for (const xAOD::Jet *jet : *jetContainer)
      {
        try
        {
          m_gnnTool->decorate(*jet);
        }
        catch (std::logic_error const &)
        {
          ATH_MSG_WARNING(
              "GNNTool returned an error due to: invalid particle link");
          SG::AuxElement::Decorator<float> defaultDecorator("GN2ej_pdisp");
          defaultDecorator(*jet) = -1.0;
        }
      }
    }

    return StatusCode::SUCCESS;
  }

}
