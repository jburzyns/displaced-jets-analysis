/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// JetVertexDecoratorAlg.h
//
// This is an algorithm to decorate reco-level jets with
// reco-level vertex quantities
//
// Author: Jackson Burzynski <jackson.carl.burzynski@cern.ch>
///////////////////////////////////////////////////////////////////

// Always protect against multiple includes!
#ifndef EJSANALYSIS_VERTEXDECORATORALG
#define EJSANALYSIS_VERTEXDECORATORALG

#include <AthContainers/ConstDataVector.h>
#include <AthenaBaseComps/AthHistogramAlgorithm.h>
#include <SystematicsHandles/SysReadHandle.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODTracking/VertexContainer.h>

class TH3S;
template<typename T>
class TMatrixT;

namespace DJ
{

  /// \brief An algorithm for decorating jets with custom observables
  class VertexDecoratorAlg final : public AthHistogramAlgorithm
  {
public:
    /// \brief The standard constructor
    VertexDecoratorAlg(const std::string &name, ISvcLocator *pSvcLocator);

    /// @brief Initialisation method, for setting up tools and other persistent
    /// configs
    StatusCode initialize() override;
    /// \brief Execute method, for actions to be taken in the event loop
    StatusCode execute() override;
    /// We use default finalize() -- this is for cleanup, and we don't do any

    // functions
private:
    
    int passMaterial(const xAOD::Vertex* vtx  );
    int passMaterialStrict(const xAOD::Vertex* vtx  );


    // configurables
private:
    /// @brief the name of the vertex container to use
    SG::ReadHandleKey<ConstDataVector<xAOD::VertexContainer>>
        m_vertexContainerKey{this, "vertexContainerKey",
                             "VrtSecInclusive_SecondaryVertices",
                             "xAOD::Vertex container to use"};
    /// @brief the name of the EventInfo object
    SG::ReadHandleKey<xAOD::EventInfo> m_eventInfoKey{
        this, "eventInfoKey", "EventInfo", "xAOD::EventInfo container to use"};

    /// @brief the suffix to add to the decorations
    Gaudi::Property<std::string> m_deco_suffix{
        this, "suffix", "", "Suffix to add after the decoration"};

    /// @brief do we calculate material veto flags ?
    Gaudi::Property<bool> m_add_materialVetoFlag{
        this, "addMaterialVetoFlag", false, ""};

    /// @brief file name of inner material map
    Gaudi::Property<std::string> m_matMapInnerFile{this, "materialMapInnerFile", "", ""};
    /// @brief file name of outer material map
    Gaudi::Property<std::string> m_matMapOuterFile{this, "materialMapOuterFile", "", ""};
    /// @brief  name of inner material map
    Gaudi::Property<std::string> m_matMapInnerName{this, "materialMapInnerName", "FinalMap_inner", ""};
    /// @brief  name of outer material map
    Gaudi::Property<std::string> m_matMapOuterName{this, "materialMapOuterName", "matmap_outer", ""};
    /// @brief  name of outer material map
    Gaudi::Property<std::string> m_matMapMatrixName{this, "materialMapMatrix", "FoldingInfo", ""};
    
    
    // internals
private:
    std::unordered_map<std::string, SG::AuxElement::Decorator<float>>
        m_decos_float;
    std::unordered_map<std::string, SG::AuxElement::Decorator<int>>
        m_decos_int;
    // clang-format off
    std::vector<std::string> m_vars_int{
      "ntrk",
      "passMatVeto"
    };
    std::vector<std::string> m_vars_float{
      "r",
      "Lxy",
      "Lxy_err",
      "Lxy_sig",
      "pt",
      "eta",
      "phi",
      "m",
      "maxDeltaR",
      "mu",
      "HT",
      "mind0",
      "maxd0",
      "charge",
    };
    // clang-format on


  // Data for material veto flag
    TH3S* m_materialMapOuter;
    TH3S* m_materialMapInner;
    TMatrixT<double>* m_materialMapMatrix;

  };




}

#endif
