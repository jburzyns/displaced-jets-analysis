from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from AnaAlgorithm.DualUseConfig import createAlgorithm
from FlavorTagDiscriminants.FlavorTagNNConfig import GNNToolCfg
from EasyjetHub.output.ttree.branch_manager import BranchManager, SystOption
from EasyjetHub.output.ttree.electrons import get_electron_branches
from EasyjetHub.output.ttree.photons import get_photon_branches
from EasyjetHub.output.ttree.muons import get_muon_branches
from EasyjetHub.output.ttree.taus import get_tau_branches
from EasyjetHub.output.ttree.minituple_config import add_passes_OR_branch


def displaced_jet_cfg(flags):
    cfg = ComponentAccumulator()

    input_name = flags.Analysis.container_names.input.reco4EMTopoJet
    calib_name = flags.Analysis.container_names.allcalib.reco4EMTopoJet

    # combine the two ghost track collections into one
    jetGhostMergingAlg = createAlgorithm(
        "CP::JetGhostMergingAlg", "DisplacedJet_JetGhostMerger"
    )
    jetGhostMergingAlg.JetCollection = input_name
    jetGhostMergingAlg.MergedGhostName = f"{input_name}.GhostTrackLRTMerged"
    jetGhostMergingAlg.InputGhostTrackNames = ["GhostTrack", "GhostTrackLRT"]
    cfg.addEventAlgo(jetGhostMergingAlg)

    # add necessary decorations to standard and LRT tracks
    cfg.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            "SimpleTrackAugmenter",
            trackContainer="InDetTrackParticles",
            primaryVertexContainer="",
            prefix="btagIp_",
        )
    )
    cfg.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            "SimpleTrackAugmenter_LRT",
            trackContainer="InDetLargeD0TrackParticles",
            primaryVertexContainer="",
            prefix="btagIp_",
        )
    )

    # compute the GNN scores
    if flags.Analysis.do_gnn:
        gnnTool = cfg.getPrimaryAndMerge(
            GNNToolCfg(
                flags,
                NNFile=flags.Analysis.model_path,
                trackLinkType="IPARTICLE",
                variableRemapping={"BTagTrackToJetAssociator": "GhostTrackLRTMerged"},
            )
        )
        cfg.addEventAlgo(
            CompFactory.DJ.GNNDecoratorAlg(
                "DisplacedJet_GNNDecoratorAlg",
                jetContainerKey=calib_name,
                gnnTool=gnnTool,
            )
        )

    # decorate jets with some reco vertex information
    if flags.Analysis.do_vertices:
        # directly convert the vertexin config in yaml into dict :
        vtxflags = flags.Analysis.vertexing.as_mutable()
        cfg.addEventAlgo(
            CompFactory.DJ.VertexDecoratorAlg(
                "VertexDecoratorAlg",
                vertexContainerKey=flags.Analysis.container_names.output.vertices,
                **vtxflags,
            )
        )

    return cfg


def displaced_jet_branches(flags, tree_flags):
    branches = []

    # Additionnal EventInfo
    branches += get_eventInfo_branches(flags)

    # Vertices
    if tree_flags.reco_outputs.vertices:
        branches += get_vertex_branches(flags, tree_flags.reco_outputs.vertices, "vtx")

    # Jet
    if tree_flags.reco_outputs.small_R_jets:
        branches += get_displaced_jet_branches(
            flags,
            tree_flags,
            tree_flags.reco_outputs.small_R_jets,
            "recojet_antikt4",
        )

    # Leptons with WPs
    objflags = {x: f'do_{x}' for x in ['electrons', 'photons', 'muons', 'taus']}
    objects_out = {
        "electrons": ("Electron", "el", get_electron_branches),
        "photons": ("Photon", "ph", get_photon_branches),
        "muons": ("Muon", "mu", get_muon_branches),
        "taus": ("Tau", "tau", get_tau_branches),
    }
    for objtype, (obj_config, prefix, branch_getter) in objects_out.items():
        write_container_flag = tree_flags.reco_outputs[objtype]
        if flags.Analysis[objflags[objtype]] and write_container_flag:
            # Default wp
            default_wp = f"{flags.Analysis[obj_config].ID}_{flags.Analysis[obj_config].Iso}"
            write_container = default_wp + write_container_flag
            branches += branch_getter(
                flags,
                tree_flags,
                input_container=write_container,
                output_prefix=f"{prefix}_{default_wp}",
            )
            if flags.Analysis.do_overlap_removal:
                branches.append(add_passes_OR_branch(write_container, f"{prefix}_{default_wp}"))

            # Extra wps
            if flags.Analysis[obj_config].extra_wps:
                for wp in flags.Analysis[obj_config].extra_wps:
                    extra_wp = f"{wp[0]}_{wp[1]}"
                    write_container = extra_wp + write_container_flag
                    branches += branch_getter(
                        flags,
                        tree_flags,
                        input_container=write_container,
                        output_prefix=f"{prefix}_{extra_wp}",
                    )

    return branches


def get_eventInfo_branches(flags):
    eventinfo_branches = BranchManager(
        input_container="EventInfo",
        output_prefix="",
    )

    dj_event_vars = []

    eventinfo_branches.variables += dj_event_vars

    return eventinfo_branches.get_output_list()


def get_vertex_branches(flags, input_container, output_prefix):
    vertex_branches = BranchManager(
        input_container,
        output_prefix,
        do_overlap_removal=False,
        systematics_option=SystOption.NONE,
    )

    vertex_branches.add_four_mom_branches(do_mass=True)

    vertex_branches.variables += [
        "x",
        "y",
        "z",
        "r",
        "Lxy",
        "Lxy_err",
        "Lxy_sig",
        "maxDeltaR",
        "mu",
        "HT",
        "mind0",
        "maxd0",
        "ntrk",
        "charge",
        "passMatVeto",
    ]

    return vertex_branches.get_output_list()


def get_displaced_jet_branches(flags, tree_flags, input_container, output_prefix):
    displaced_jet_branches = BranchManager(
        input_container,
        output_prefix,
        do_overlap_removal=flags.Analysis.do_overlap_removal,
        systematics_option=SystOption.ALL_SYST,
    )

    displaced_jet_branches.add_four_mom_branches(do_mass=True)

    if flags.Analysis.do_gnn:
        displaced_jet_branches.variables += ["GN2ej_pdisp"]

    return displaced_jet_branches.get_output_list()
