# Network documentations

## GN2ej_20231022-T111515_network.onnx

- Trained with custom LLP1, full truth contents
- Include full substructure variables
- 200 tracks per jet
- Trained on resampled background jets
- 1M signal, 1M background roughly

## GN2ej_substr_20231021-T185330_network.onnx

- Trained with custom LLP1, full truth contents
- Include full substructure variables
- 200 tracks per jet
- Trained on resampled background jets
- 1M signal, 1M background roughly
- Includes all jet substructure variables
