## Basic usage

The main executable in this package is the `dj-ntupler` script. The basic usage is:
```
dj-ntupler myInputFile.DAOD_LLP1.pool.root -c [path-to-config] --evtMax 10 --out-file analysis-ntuple.root
```
The default config file is located in `DJAnalysis/share/RunConfig-LLP1-dj.yaml`.
There is no need to specify if you are running over data or MC, this information is automatically read 
from the sample metadata.

To see a full list of runtime options, you can run `dj-ntupler --help`.

## Grid submission

To process files on the grid, start by sourcing the setup script. From your `run`
directory, do
```
source ../displaced-jets-analysis/setup-grid.sh
```
The `easyjet-gridsubmit` script is provided to facilitate grid submissions. 

Usage:
```
usage: easyjet-gridsubmit [-h] [--mc-list MC_LIST] [--data-list DATA_LIST] --run-config RUN_CONFIG [--exec EXEC] [--campaign CAMPAIGN] [--asetup ASETUP] [--dest-se DEST_SE]
                          [--excluded-site EXCLUDED_SITE] [--nGBperJob NGBPERJOB] [--noSubmit]
optional arguments:
  -h, --help            show this help message and exit
  --mc-list MC_LIST     Text file containing MC datasets
  --data-list DATA_LIST
                        Text file containing data datasets
  --run-config RUN_CONFIG
                        file path to the runConfig
  --exec EXEC           The name of the executable
  --campaign CAMPAIGN   The name of the campaign. Will be prepended to the output dataset name. Can include datetime.strftime replacement fields
  --asetup ASETUP       The asetup command, in case it cannot be read from the environment
  --dest-se DEST_SE     An RSE to duplicate the results to, for example 'DESY-ZN_LOCALGROUPDISK'
  --excluded-site EXCLUDED_SITE
                        Any sites to exclude when running
  --nGBperJob NGBPERJOB
                        How many GB required per job
  --noSubmit            Don't submit the jobs, just create the tarball
```
where `--exec` should be `--exec dj-ntupler` to run our analysis executable.
Example:
```
easyjet-gridsubmit --mc-list ../displaced-jets-analysis/DJAnalysis/datasets/signal.txt --run-config ../displaced-jets-analysis/DJAnalysis/share/RunConfig-LLP1-dj.yaml --campaign %Y_%m_%d --exec dj-ntupler
```

## Datasets

### Signal

#### MC23a/d

### Background

#### MC23a
| DSID   | DAOD_LLP1                                                                                            |
|--------|------------------------------------------------------------------------------------------------------|
| 801167 |mc23_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801168 |mc23_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801169 |mc23_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801170 |mc23_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801171 |mc23_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801172 |mc23_13p6TeV.801172.Py8EG_A14NNPDF23LO_jj_JZ7.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801173 |mc23_13p6TeV.801173.Py8EG_A14NNPDF23LO_jj_JZ8.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027     |
| 801174 |mc23_13p6TeV.801174.Py8EG_A14NNPDF23LO_jj_JZ9incl.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 700780 |mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 700781 |mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 700782 |mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 700783 |mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 700784 |mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 700785 |mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_LLP1.e8514_s4162_r14622_p6027 |
| 601229 |mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_LLP1.e8514_s4162_r14622_p6057 |
| 601230 |mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_LLP1.e8514_s4162_r14622_p6057 |
| 601237 |mc23_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_LLP1.e8514_s4162_r14622_p6057 |

#### MC23d
| DSID   | DAOD_LLP1                                                                                            |
|--------|------------------------------------------------------------------------------------------------------|
| 801167 |mc23_13p6TeV.801167.Py8EG_A14NNPDF23LO_jj_JZ2.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801168 |mc23_13p6TeV.801168.Py8EG_A14NNPDF23LO_jj_JZ3.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801169 |mc23_13p6TeV.801169.Py8EG_A14NNPDF23LO_jj_JZ4.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801170 |mc23_13p6TeV.801170.Py8EG_A14NNPDF23LO_jj_JZ5.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801171 |mc23_13p6TeV.801171.Py8EG_A14NNPDF23LO_jj_JZ6.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801172 |mc23_13p6TeV.801172.Py8EG_A14NNPDF23LO_jj_JZ7.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801173 |mc23_13p6TeV.801173.Py8EG_A14NNPDF23LO_jj_JZ8.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027     |
| 801174 |mc23_13p6TeV.801174.Py8EG_A14NNPDF23LO_jj_JZ9incl.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 700780 |mc23_13p6TeV.700780.Sh_2214_Wmunu_maxHTpTV2_BFilter.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 700781 |mc23_13p6TeV.700781.Sh_2214_Wmunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 700782 |mc23_13p6TeV.700782.Sh_2214_Wmunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 700783 |mc23_13p6TeV.700783.Sh_2214_Wtaunu_maxHTpTV2_BFilter.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 700784 |mc23_13p6TeV.700784.Sh_2214_Wtaunu_maxHTpTV2_CFilterBVeto.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 700785 |mc23_13p6TeV.700785.Sh_2214_Wtaunu_maxHTpTV2_CVetoBVeto.deriv.DAOD_LLP1.e8514_s4159_r15224_p6027 |
| 601229 |mc23_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_LLP1.e8514_s4159_r15224_p6057 |
| 601230 |mc23_13p6TeV.601230.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_LLP1.e8514_s4159_r15224_p6057 |
| 601237 |mc23_13p6TeV.601237.PhPy8EG_A14_ttbar_hdamp258p75_allhad.deriv.DAOD_LLP1.e8514_s4159_r15224_p6057 |


### Data

| Year        | DAOD_LLP1                                                                    |
| ----------- | ---------------------------------------------------------------------------- |
| 2022        | data22_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_LLP1.grp22_v01_p6000 |
| 2023        | data23_13p6TeV.periodAllYear.physics_Main.PhysCont.DAOD_LLP1.grp23_v01_p6000 |

