## Getting started

In a clean working directory (`$WORKDIR`), clone the repository locally:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/jburzyns/displaced-jets-analysis.git
```
Note the `--recursive` argument, which is needed to get the submodules in the package.

Now, compile the package
```
mkdir build
cd build
source ../displaced-jets-analysis/setup.sh
cmake ../displaced-jets-analysis/
make
source */setup.sh
```

## Future setups with same install

On future sessions, you only need to run

```
cd $WORKDIR/build
setupATLAS
asetup --restore
source */setup.sh
```

## Updating the submodule (expert)

You may occasionally have to resync the submodules after an update. You can run
```
git submodule update --init --recursive
```
When changing branches with `git checkout` or `git switch`, also be sure to use the `--recurse-submodule` option to keep the submodules current.

