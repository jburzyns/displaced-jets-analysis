# Displaced Jet Analysis 

[![pipeline status](https://gitlab.cern.ch/jburzyns/displaced-jets-analysis/badges/main/pipeline.svg)](https://gitlab.cern.ch/jburzyns/displaced-jets-analysis/-/commits/main) [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Welcome to the Run 3 Emerging Jets Analysis software. 
Documentation is available here:

[![DJ docs](https://img.shields.io/badge/info-documentation-informational)](https://atlas-displaced-jets-run3.docs.cern.ch/)

A quickstart guide is below if you do not want to read the documentation.

## Installation

In a clean working directory (`$WORKDIR`), clone the repository locally:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/jburzyns/displaced-jets-analysis.git
```
Now, compile the package
```
mkdir build
cd build
source ../displaced-jets-analysis/setup.sh
cmake ../displaced-jets-analysis/
make
source */setup.sh
```

## Running

The main executable in this package is the `dj-ntupler` script. The basic usage is:
```
dj-ntupler myInputFile.DAOD_LLP1.pool.root --runConfig [path-to-config] --evtMax 10 --out-file analysis-ntuple.root
```
There is no need to specify if you are running over data or MC, this information is automatically read 
from the sample metadata.

To see a full list of runtime options, you can run `dj-ntupler --help`.

